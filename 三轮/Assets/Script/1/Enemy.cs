using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Transform targetleft;
    public Transform targetright;
    private float timer;
    private bool Isok;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timer = Time.deltaTime;
        if (timer == 0)
        {
            transform.Translate(0, -0.003f, 0);
        }
        if (timer > 0)
        {
            if (transform.position.x <= targetleft.position.x)
            {
                Isok = true;
            }

            if (transform.position.x > targetright.position.x)
            {
                Isok = false;
            }
            if (Isok)
            {
                transform.Translate(0.003f, 0, 0);
            }
            if (!Isok)
            {
                transform.Translate(-0.003f, 0, 0);
            }
        }

    }
}
