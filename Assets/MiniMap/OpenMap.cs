using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenMap : MonoBehaviour
{
    public GameObject Map;
    private bool isOpen;

     void Update()
    {
        if (MapReco.canOpenMap)
        {
            if (Input.GetKeyDown(KeyCode.M))
            {
                isOpen = !isOpen;
                if (isOpen)
                    Map.SetActive(true);
                else
                    Map.SetActive(false);
            }
        }
    }
}
