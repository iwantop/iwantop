using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapReco : MonoBehaviour
{
    public static bool canOpenMap;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            canOpenMap = true;
        }
    }
}
