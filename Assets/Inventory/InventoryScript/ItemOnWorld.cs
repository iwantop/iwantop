using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemOnWorld : MonoBehaviour
{
    public Item thisItem;
    public Inventory myBag1;
    public GameObject TriggerDialog;
    public MonoBehaviour move;//浏览菜单栏时无法移动

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            AddNewItem();
            TriggerDialog.SetActive(true);
            GameManager.instance.shouji++;
            //Destroy(gameObject);
            gameObject.SetActive(false);
            move.enabled = false;
        }
    }

    public void AddNewItem()
    {
        if (!myBag1.itemList.Contains(thisItem))
        {
            //myBag1.itemList.Add(thisItem);
            //InventoryManager.CreateNewItem(thisItem);
            for (int i = 0; i < myBag1.itemList.Count; i++)//通过循环将物品添加至列表空格
            {
                if (myBag1.itemList[i] == null)
                {
                    myBag1.itemList[i] = thisItem;
                    break;
                }
            }
        }

        InventoryManager.RefreshItem();//拾取后刷新列表
    }
}

