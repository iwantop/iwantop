using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    public int slotID;//slot对应列表中的位置
    public Item slotItem;
    public Image slotImage;
    public string slotInfo;//文本信息
    public string slotDetail;//详情
    public GameObject itemInSlot;
    public Sprite slotSprite;
    public bool ifGun;
    public bool ifFu;
    public bool ifFire;
    public void SetUpSlot(Item item)//设置格中相关信息
    {
        if (item == null)
        {
            itemInSlot.SetActive(false);
            return;
        }
        slotImage.sprite = item.itemImage;
        slotSprite = item.itemImage;
        slotInfo = item.itemInfo;
        slotDetail = item.itemDetail;
        ifGun = item.Gun;
        ifFu = item.Fu;
        ifFire = item.Fire;

    }
    /*
    private void Update()
    {
        if (ButtonManager.ifRestart)
        {
            itemInSlot = null;
            slotItem = null;
            //slotImage.sprite = null;
            slotSprite = null;
            slotInfo = null;
            slotDetail = null;
        }
    }
    */
    public void ItemOnClicked()
    {
        InventoryManager.UpdateItemInfo(slotInfo);
        InventoryManager.ShowDetail(slotDetail,slotSprite);
        if (ifGun)
        {
            GameManager.instance.gunState = true;
            GameManager.instance.nullState = false;
            GameManager.instance.fuState = false;
            GameManager.instance.fireState = false;
        }
        if (ifFu)
        {
            GameManager.instance.gunState = false;
            GameManager.instance.nullState = false;
            GameManager.instance.fuState = true;
            GameManager.instance.fireState = false;
        }
        if (ifFire)
        {
            GameManager.instance.gunState = false;
            GameManager.instance.nullState = false;
            GameManager.instance.fuState = false;
            GameManager.instance.fireState = true;
        }
    }
    

}
