using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenBag : MonoBehaviour
{
    public GameObject Bag;
    bool isOpen;
    public static bool bagIsOpen;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.B))
        {           
            isOpen = !isOpen;
            if (isOpen)
            {
                Bag.SetActive(true);
                bagIsOpen = true;
            }
            else
            {
                Bag.SetActive(false);
                bagIsOpen = false;
            }
        
        }
    }
}


