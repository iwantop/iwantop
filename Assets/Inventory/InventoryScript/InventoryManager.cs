using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    static InventoryManager instance;

    public Inventory myBag;
    public GameObject slotGrid;
    public GameObject detailPanel;
    //public Slot slotPrefeb;
    public GameObject emptySlot;
    public GameObject detailButton;
    public Text itemInfo;
    public Text itemDetail;
    public Image detailImage;

    public List<GameObject> slots = new List<GameObject>();
    void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        instance = this;
    }

    private void OnEnable()
    {
        RefreshItem();
        instance.itemInfo.text = "";
    }

    void Update()
    {
        if (OpenBag.bagIsOpen==false)//使背包下方介绍在背包关闭后清空
        {
            instance.itemInfo.text = "";
            instance.detailButton.SetActive(false);
        }
    }
    public static void UpdateItemInfo(string itemDescription)//更新背包下方的介绍
    {
        instance.itemInfo.text = itemDescription;
        instance.detailButton.SetActive(true);
    }

    public void OpenDetail()//打开详情panel
    {
        detailPanel.SetActive(true);
        
    }

    public static void ShowDetail(string detail,Sprite image)//在对应区域加载文字，text与panel同时显示
    {
        instance.itemDetail.text = detail;
        instance.detailImage.sprite = image;
    }

    /*public static void CreateNewItem(Item item)
    {
        Slot newItem = Instantiate(instance.slotPrefeb, instance.slotGrid.transform.position, Quaternion.identity);
        newItem.gameObject.transform.SetParent(instance.slotGrid.transform,false);
        newItem.slotItem = item;
        newItem.slotImage.sprite = item.itemImage;
    }*/

    public static void RefreshItem()//刷新列表中物品信息
    {
        //循环删除Grid子集物品
        for(int i=0; i < instance.slotGrid.transform.childCount; i++)
        {
            if (instance.slotGrid.transform.childCount == 0)
                break;
            Destroy(instance.slotGrid.transform.GetChild(i).gameObject);
            instance.slots.Clear();
        }
        //循环生成Bag里的Slot
        for(int i = 0; i < instance.myBag.itemList.Count; i++)
        {
            //CreateNewItem(instance.myBag.itemList[i]);
            instance.slots.Add(Instantiate(instance.emptySlot));//创建格子
            instance.slots[i].transform.SetParent(instance.slotGrid.transform);//加到父级下
            instance.slots[i].GetComponent<Slot>().slotID = i;//设置位置序号
            instance.slots[i].GetComponent<Slot>().SetUpSlot(instance.myBag.itemList[i]);//设置格中相关内容
        }
    }
    
}
