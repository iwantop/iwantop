using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Close : MonoBehaviour
{
    public GameObject panel;
    public MonoBehaviour move;
    void OnGUI()
    {
        Event e = Event.current;
        move.enabled = true;
        if (e.isKey)
        {
            //Destroy(panel,0.3f);
            panel.SetActive(false);
            this.enabled = false;
        }
    }
}

