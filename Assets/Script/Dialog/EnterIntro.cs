using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterIntro : MonoBehaviour
{
    public GameObject IntroducePanel;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            IntroducePanel.SetActive(true);

        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

            IntroducePanel.SetActive(false);
        }
    }
}
