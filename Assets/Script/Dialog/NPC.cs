using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    public GameObject TriggerDialog;
    public MonoBehaviour move;

    
        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {

                TriggerDialog.SetActive(true);
                
                move.enabled = false;
            }
        }
    
}
