using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterToIntroduce : MonoBehaviour
{
    public GameObject IntroducePanel;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            IntroducePanel.SetActive(true);
        }
    }
   
}
