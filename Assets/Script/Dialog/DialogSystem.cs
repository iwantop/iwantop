using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogSystem : MonoBehaviour
{
    [Header("UI组件")]
    public Text textLabel;//Panel下的text组件
    public Image faceImage;//Panel下的image组件(用于显示对话角色)

    [Header("文本文件")]
    public TextAsset textFile;//txt文件（显示内容）
    public int index;//编号
    public float textSpeed;//显示速度

    [Header("头像")]
    public Sprite face01, face02, face03;

    bool textFinished;//是否完成打字
    bool cancelTyping;//取消打字

    List<string> textList = new List<string>();//新建字符型集合（列表）

    void Awake()
    {
        GetTextFromFile(textFile);
    }
    private void OnEnable()
    {
        textFinished = true;
        StartCoroutine(SetTextUI());
    }

    void Update()
    {
        DialogRunning();
    }

    void GetTextFromFile(TextAsset file)//获取TXT文本
    {
        textList.Clear();
        index = 0;

        var lineData = file.text.Split('\n');//将文本根据换行进行分割

        foreach (var line in lineData)//将每行数据进行储存
        {
            textList.Add(line);
        }
    }

    IEnumerator SetTextUI()
    {
        textFinished = false;
        textLabel.text = "";

        switch (textList[index].Trim().ToString())//将TXT文本中ABC替换为相应image
        {
            case "A":
                faceImage.sprite = face01;
                index++;
                break;
            case "B":
                faceImage.sprite = face02;
                index++;
                break;
            case "C":
                faceImage.sprite = face03;
                index++;
                break;
        }

        int letter = 0;
        while (!cancelTyping && letter < textList[index].Length - 1)//一个字一个字打印
        {
            textLabel.text += textList[index][letter];
            letter++;
            yield return new WaitForSeconds(textSpeed);
        }
        textLabel.text = textList[index];
        cancelTyping = false;
        textFinished = true;
        index++;
    }

    void DialogRunning()
    {
        if (Input.GetKeyDown(KeyCode.E) && index == textList.Count)//关闭对话框
        {
            gameObject.SetActive(false);
            Time.timeScale = 1;
            index = 0;
            return;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (textFinished && !cancelTyping)//未取消输出
            {
                StartCoroutine(SetTextUI());
            }
            else if (!textFinished && !cancelTyping)//取消文字输出，直接显示该行全部文本
            {
                cancelTyping = true;
            }
        }
    }
}
