using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
//���
    public GameObject Left;
    //public Collider2D Collider;

//�ı���ɫ
    public GameObject attacher;
    private SpriteRenderer playersprite;
    private Color color;
    private  Color cur = new Color(0.482f, 0.227f, 0.227f, 1);
    private  Color pre;
    private float co = 1.0f;

    void OnEnable()
    {
        playersprite = attacher.GetComponent<SpriteRenderer>();
        pre = playersprite.color;
    }

    void Update()
    {
        if(Left == null)
        {
            co *= 0.94f;
            //Destroy(Collider, 0.3f);
            //Invoke("ChangeColor", 0.5f);
            ChangeColor();
        }                
    }

    public void ChangeColor()
    {
        //color = new Color(0.482f, 0.227f, 0.227f, 1);
        color = Color.Lerp(pre, cur, 1.0f-co);
        playersprite.color = color;
    }
}
