using UnityEngine;
using System.Collections;

public class LightEvent : LightEventListener
{
    public Material M1,M2;
    public GameObject Object;

    protected override void OnLightEnter(RaycastHit info)
    {
        //Debug.Log(info.collider.name + "Enter");

    }

    protected override void OnLightStay(RaycastHit info)
    {
        //Debug.Log(info.collider.name + "Stay");
        //GameObject.Find("Item").transform.Find("PoSunZhiZhang").GetComponent<Renderer>().material = M1;     
        if (Object != null)
            Object.GetComponent<Renderer>().material = M1;
        //this.transform.GetComponent<Renderer>().material = M1;
    }

    protected override void OnLightExit(RaycastHit info)
    {
        //Debug.Log(info.collider.name + "Exit");
        //GameObject.Find("Item").transform.Find("PoSunZhiZhang").GetComponent<Renderer>().material = M2;
        if (Object != null)
            Object.GetComponent<Renderer>().material = M2;
        //this.transform.GetComponent<Renderer>().material = M2;
    }

}
