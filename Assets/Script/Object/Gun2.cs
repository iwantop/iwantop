using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun2 : MonoBehaviour
{
    public Transform fm;
    public GameObject bigBullet;
    public GameObject player;
    public Animator animator;
    public Text text;
    
    private float time;
    public float interval;
    public int bu;
    float rotz;
    Vector2 mousePos;
    Vector2 direction;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (time > 0)
            time -= Time.deltaTime;
        
        fire();
    }
    void fire()
    {
        if (bu > 0&&time<=0&&Input.GetMouseButtonDown(1))
        {
            
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            direction = new Vector2(mousePos.x - fm.position.x, mousePos.y - fm.position.y);
            
            animator.SetTrigger("Shake");
            rotz = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            GameObject bullet = Instantiate(bigBullet, fm.position, Quaternion.Euler(new Vector3(0, 0, rotz)));
            Vector3 differ = new Vector3(direction.x, direction.y, 0);
            player.transform.position = player.transform.position - differ.normalized*0.5f;
            bullet.GetComponent<Bullet>().SetSpeed(direction);
            SoundManager.FireGunAudio();
            bu--;
        }
    }
    public void save()
    {
        PlayerPrefs.SetInt("B", bu);
        PlayerPrefs.Save();
    }
    public void load()
    {
        bu = PlayerPrefs.GetInt("B");
    }
}
