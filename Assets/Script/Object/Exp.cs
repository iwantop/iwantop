using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exp : MonoBehaviour
{
    private Animator animator;

    private AnimatorStateInfo info;//动画播放进度
    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        info = animator.GetCurrentAnimatorStateInfo(0);//持续获取动画进度
        if (info.normalizedTime >= 1)
            Destroy(gameObject);
    }
}
