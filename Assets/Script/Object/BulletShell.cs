using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShell : MonoBehaviour
{
    public float speed;
    public float stopTime;
    public float fadeSpeed;
    private Rigidbody2D rb;
    private SpriteRenderer sp;
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        sp = GetComponent<SpriteRenderer>();
        float angel = Random.Range(-30f, 30f);

        rb.velocity =Quaternion.AngleAxis(angel,Vector3.forward)*Vector2.up * speed;
        
        StartCoroutine(Stopt());
    }
    IEnumerator Stopt()
    {
        yield return new WaitForSeconds(stopTime);
        rb.velocity = Vector2.zero;
        rb.gravityScale = 0;

        while (sp.color.a > 0)//阿尔法值大于0，逐渐变透明
        {
            sp.color = new Color(sp.color.r, sp.color.g, sp.color.g, sp.color.a - fadeSpeed);
            yield return new WaitForFixedUpdate();//变透明后等待一个FixedUpdate帧
        }
        Destroy(gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.back* speed);
    }
}
