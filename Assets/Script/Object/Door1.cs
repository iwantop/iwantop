using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door1 : MonoBehaviour
{
    public bool canOpen;
    Rigidbody2D rb;
    Vector2 tr;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        tr = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = tr;
    }

    void Open()
    {
        canOpen = false;
        transform.eulerAngles = transform.eulerAngles + new Vector3(0, 0, 70);
        //rb.bodyType = RigidbodyType2D.Dynamic;
        //rb.gravityScale = 0;
        SoundManager.DoorOpenAudio();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" && canOpen)
            Open();
    }
}
