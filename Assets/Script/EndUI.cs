using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndUI : MonoBehaviour
{
    public GameObject[] texts;
    public GameObject button1;
    public GameObject button2;
    void Start()
    {
        StartCoroutine(s());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator s()
    {
        foreach (var item in texts)
        {
            item.SetActive(true);
            yield return new WaitForSeconds(5.3f);
        }
        button1.SetActive(true);
        button2.SetActive(true);
    }
}
