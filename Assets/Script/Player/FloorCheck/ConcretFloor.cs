using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConcretFloor : MonoBehaviour
{
    public float num = 0;

    private void Update()
    {
        if ((num * 0.5) % 2 != 0)
        {
            FloorState.isConcrete = true;
        }
        else if ((num * 0.5) % 2 == 0)
        {
            FloorState.isConcrete = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FloorCheck"))
        {
            num++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FloorCheck"))
        {
            num++;
        }
    }
}
