using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseFloor3 : MonoBehaviour
{
    public float num = 0;

    private void Update()
    {
        if (num < 2)
        {
            FloorState.isHouse03 = true;
        }
        else if (num >= 2)
        {
            if ((num * 0.5) % 2 == 0)
            {
                FloorState.isHouse03 = true;
            }
            else if ((num * 0.5) % 2 != 0)
            {
                FloorState.isHouse03 = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FloorCheck"))
        {
            num++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FloorCheck"))
        {
            num++;
        }
    }
}
