using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseFloor : MonoBehaviour
{
    public static float num1 = 0;
    public GameObject Collider;

    private void Update()
    {
        if ((num1 * 0.5) % 2 != 0)
        {
            FloorState.isGrass01 = false;
            FloorState.isHouse01 = true;
        }
        else if ((num1 * 0.5) % 2 == 0)
        {
            FloorState.isGrass01 = true;
            FloorState.isHouse01 = false;
        }

        if (TransCheck.TransDown)
        {
            Collider.SetActive(true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FloorCheck"))
        {
            num1++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FloorCheck"))
        {
            num1++;
        }
    }
}
