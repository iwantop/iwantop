using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfectionFloor : MonoBehaviour
{
    public float num = 0;

    private void Update()
    {
        if ((num) % 2 != 0)
        {
            FloorState.isInfection = true;
        }
        else if ((num) % 2 == 0)
        {
            FloorState.isInfection = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FloorCheck"))
        {
            num++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FloorCheck"))
        {
            num++;
        }
    }
}
