using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseFloor4 : MonoBehaviour
{
    public static float num4 = 0;

    private void Update()
    {
        if (num4 >= 2)
        {
            FloorState.isHouse04 = true;
        }
        if (num4 == 0)
        {
            FloorState.isHouse04 = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FloorCheck"))
        {
            num4++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FloorCheck"))
        {
            num4++;
        }
    }
}
