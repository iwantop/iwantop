using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransCheck : MonoBehaviour
{
    public static bool TransDown;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            TransDown = true;
    }
}
