using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shot : MonoBehaviour
{
    public int bulletNum;
    [SerializeField] int sb=30;

    public Text bn;

    public GameObject bulletPrefab;

    public GameObject shellPrefab;

    public Transform muzzlePos;

    public Transform shellPos;

    private Vector2 mousePos;

    private Vector2 direction;
    float rotz;

    //save
    const string PLAYER_DATA_KEY = "playerData";
    const string PLAYER_DATA_NAME = "playerData.cunchu";
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bn.text = "Bullet：" + bulletNum;
        
    }
    
    void Fire()
    {
        if (bulletNum > 0)
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            direction = new Vector2(mousePos.x - muzzlePos.position.x, mousePos.y - muzzlePos.position.y);
            rotz = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            GameObject bullet = Instantiate(bulletPrefab, muzzlePos.position, Quaternion.Euler(new Vector3(0, 0, rotz)));
            GameObject shell = Instantiate(shellPrefab, shellPos.position, Quaternion.identity);
            bullet.GetComponent<Bullet>().SetSpeed(direction);
            SoundManager.ShotAudio();
            bulletNum--;
            sb--;
        }
    }
    void SaveByJson()
    {
        SaveSystem.SaveByJson(PLAYER_DATA_NAME,sb);
    }
    void LoadFromJson()
    {
        var savebullet = SaveSystem.LoadFromJson<int>(PLAYER_DATA_NAME);//读取存入文件中的数据，是整形的弹药数
        sb = savebullet;
    }
    void SaveByPlayerPref()
    {
        PlayerPrefs.SetInt("BulletN", bulletNum);
        PlayerPrefs.Save();
    }
    void LoadByPlayerPref()
    {
        bulletNum = PlayerPrefs.GetInt("BulletN");
    }
    public void Save()
    {
        //SaveByJson();
        SaveByPlayerPref();
    }
    public void Load()
    {
        //LoadFromJson();
        LoadByPlayerPref();
    }
    
}
