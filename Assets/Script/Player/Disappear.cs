using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disappear : MonoBehaviour
{
    private Transform attacherts;
    public GameObject attacher;
    private SpriteRenderer thisSprite;
    private SpriteRenderer playersprite;
    private Color color;

    [Header("时间控制参数")]
    public float activetime;
    public float activestart;

    [Header("不透明度控制")]
    private float alpha;
    public float alphaset;
    public float alphamuti;

    // Start is called before the first frame update
    void OnEnable()
    {
        attacherts = attacher.transform;
        thisSprite = GetComponent<SpriteRenderer>();
        playersprite = attacher.GetComponent<SpriteRenderer>();

        alpha = alphaset;

        thisSprite.sprite = playersprite.sprite;

        transform.position = attacherts.position;
        transform.localScale = attacherts.localScale;
        transform.rotation = attacherts.rotation;

        activestart = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        alpha *= alphamuti;
        color = new Color(1, 1, 1, alpha);
        thisSprite.color = color;
        if(alpha < 0.1f)
            Destroy(attacher);
    }
}
