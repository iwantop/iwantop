using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startTransAudio : MonoBehaviour
{
    public void TransAudio()
    {
        SoundManager.StartTransAudio();
    }
    public void Pause()
    {
        SoundManager.AmbientPause();
    }
}
