using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JiTan : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            SoundManager.JiTanAudio();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            SoundManager.AmbientPause();
    }
}
