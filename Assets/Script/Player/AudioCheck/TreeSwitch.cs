using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeSwitch : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        SoundManager.TreeAudio();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        SoundManager.AmbientPause();
    }
}
