using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House1 : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            SoundManager.House1Audio();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            SoundManager.AmbientPause();
    }
}
