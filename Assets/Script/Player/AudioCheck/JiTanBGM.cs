using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JiTanBGM : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            SoundManager.JiTanBk();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            SoundManager.BGMPause();
    }
}
