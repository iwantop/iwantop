using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public int moveSpeed;
    private Vector2 differ;
    public GameObject fu;
    public GameObject fg;
    public GameObject rock;
    //
    public bool Climb = false;
    public int NewSpeed = 15;
//
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        Move();
        AnimationControl();

    }
    private void AnimationControl()
    {
        if (GameManager.instance.nullState)
        {
            fu.SetActive(false);
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
                animator.SetTrigger("walk1");

            if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S))
                animator.SetTrigger("walk1b");
            fg.SetActive(false);
        }
        if (GameManager.instance.fuState)
        {
            fu.SetActive(true);
            animator.SetTrigger("stateGunb");
            animator.SetTrigger("stateFireb");
            animator.SetTrigger("stateFu");
            if (Input.GetKey(KeyCode.W))
                animator.SetTrigger("walkFu");
            if (Input.GetKey(KeyCode.S))
                animator.SetTrigger("walkFutui");
            if (!Input.GetKey(KeyCode.W))
                animator.SetTrigger("walkFub");
            if (!Input.GetKey(KeyCode.S))
                animator.SetTrigger("walkFutuib");
            if (Input.GetMouseButton(1))
            {
                animator.SetTrigger("attackFu");
                fu.SetActive(true);
            }
            if (!Input.GetMouseButton(1))
            {
                animator.SetTrigger("attackFub");
                fu.SetActive(false);
            }
            fg.SetActive(false);
        }
        if (GameManager.instance.gunState)
        {
            fu.SetActive(false);
            animator.SetTrigger("stateFub");
            animator.SetTrigger("stateFireb");
            animator.SetTrigger("stateGun");
            if (Input.GetKey(KeyCode.W))
                animator.SetTrigger("walkGun");
            if (!Input.GetKey(KeyCode.W))
                animator.SetTrigger("walkGunb");
            if (Input.GetKey(KeyCode.S))
                animator.SetTrigger("walkGuntui");
            if (!Input.GetKey(KeyCode.S))
                animator.SetTrigger("walkGuntuib");
            if (Input.GetMouseButton(1))
                animator.SetTrigger("attackGun");
            if (!Input.GetMouseButton(1))
                animator.SetTrigger("attackGunb");
            fg.SetActive(false);
        }
        if (GameManager.instance.fireState)
        {
            fu.SetActive(false);
            animator.SetTrigger("stateFub");
            animator.SetTrigger("stateGunb");
            animator.SetTrigger("stateFire");
            fg.SetActive(true);
            rock.SetActive(true);
        }
    }
    private void Move()
    {
        differ = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        if (differ.sqrMagnitude <0.7)
            return;
        if (Input.GetKey(KeyCode.W))
            transform.Translate(Vector3.up * moveSpeed *  Time.deltaTime);
        if (Input.GetKey(KeyCode.S))
            transform.Translate(Vector3.down * moveSpeed * Time.deltaTime);
        if (Input.GetKey(KeyCode.A))
            transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        if (Input.GetKey(KeyCode.D))
            transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);

        //
        if (Climb)
        {
            if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift))
                transform.Translate(Vector3.up * NewSpeed * Time.deltaTime);
            if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.LeftShift))
                transform.Translate(Vector3.down * NewSpeed * Time.deltaTime);
            if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.LeftShift))
                transform.Translate(Vector3.left * NewSpeed * 3 * Time.deltaTime);
            if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftShift))
                transform.Translate(Vector3.right * NewSpeed * 3 * Time.deltaTime);
        }
        //


        //differ = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        float rotz = Mathf.Atan2(differ.y, differ.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0,0,rotz));
        FollowMouseRotate();

        /*
        float input_H = Input.GetAxisRaw("Horizontal");
        float input_V = Input.GetAxisRaw("Vertical");

        Vector3 v = new Vector3(input_H,  input_V,0);
        v = v.normalized;                              //标准化
        v = v * moveSpeed * Time.deltaTime;           //调整移动速度
        //transform.Translate(v);                      //移动
        transform.position += v;
        //FollowMouseRotate();
        */
    }

    private void FollowMouseRotate()
    {
        //获取鼠标的坐标，鼠标是屏幕坐标，Z轴为0，这里不做转换  
        Vector3 mouse = Input.mousePosition;
        //获取物体坐标，物体坐标是世界坐标，将其转换成屏幕坐标，和鼠标一直  
        Vector3 obj = Camera.main.WorldToScreenPoint(transform.position);
        //屏幕坐标向量相减，得到指向鼠标点的目标向量，即黄色线段  
        Vector3 direction = mouse - obj;
        //将Z轴置0,保持在2D平面内  
        direction.z = 0f;
        //将目标向量长度变成1，即单位向量，这里的目的是只使用向量的方向，不需要长度，所以变成1  
        direction = direction.normalized;
        //物体自身的Y轴和目标向量保持一直，这个过程XY轴都会变化数值  
        transform.up = direction;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Tree")
        {
            Climb = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Tree")
        {
            Climb = false;
        }
    }

    public void WalkAudio()
    {
        if (FloorState.isGrass01)
            SoundManager.PlayerOnGrassAudio();
        if (FloorState.isHouse01||FloorState.isHouse02||FloorState.isHouse03||FloorState.isHouse04)
            SoundManager.PlayerOnFloorAudio();
        if (FloorState.isMud)
            SoundManager.PlayerOnMudAudio();
        if (FloorState.isInfection)
            SoundManager.PlayerOnInfectionAudio();
        if (FloorState.isConcrete)
            SoundManager.PlayerOnConcreteAudio();       
    }
    public void FuAudioPlay()
    {
        SoundManager.FuAudio();
    }
}
