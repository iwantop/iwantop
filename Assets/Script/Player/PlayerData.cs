using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour
{
    public int hp;
    public Gun2 gun2;
    const string PLAYER_DATA_KEY = "playerData1";
    const string PLAYER_DATA_NAME = "playerData1.cunchu";
    public Text text1;
    [Header("Hurt")]
    private SpriteRenderer sp;
    public float hurtLength;
    private float hurtCount;
    [System.Serializable]
    class SaveData
    {
        public int shp;
        public Vector3 playertansform;
    }
    void Start()
    {
        sp = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        HealthBar.healthCurrent = hp;
        text1.text= "Rocket shell:  " + gun2.bu;
        if (hp <= 0)
            GameManager.instance.end();
        if (hp >= 100)
            hp = 100;
        
        if (hurtCount <= 0)
            sp.material.SetFloat("_FlashAmount", 0);
        else
            hurtCount -= Time.deltaTime;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "EndGame")
            hp = 0;
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "bexp")
            hp -= 12;
    }
    public void playerDam(int i)
    {
        hp -= i;
        HurtShader();
    }
    private void HurtShader()
    {
        sp.material.SetFloat("_FlashAmount", 1);
        hurtCount = hurtLength;
    }


    SaveData SavingData()
    {
        var saveData = new SaveData();
        saveData.shp = hp;
        saveData.playertansform = transform.position;
        return saveData;
    }
    void GetData(SaveData saveData)
    {
        hp = saveData.shp;
        transform.position = saveData.playertansform;
    }
    public void save()
    {
        SaveSystem.SaveByJson(PLAYER_DATA_NAME, SavingData());
    }
    public void load()
    {
        var saveData = SaveSystem.LoadFromJson<SaveData>(PLAYER_DATA_NAME);
        GetData(saveData);
    }
}
