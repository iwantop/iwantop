using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SceneInteraction : MonoBehaviour
{
    private Animator animator;
    [Header("船")]
    public GameObject textBoat;
    public Boat boat;
    public int ib = 0;
    Vector2 boatDistance;

    [Header("出生地")]
    
    public  bool l=false;
    public GameObject floor;
    public GameObject man;
    public GameObject s;
    public GameObject AudioCollider;
    public Animator animators;
    private AnimatorStateInfo info;//动画播放进度
    public float time;//过场等待时间
    public GameObject im;
    public GameObject trigger;
    public GameObject trigger2;

    [Header("树")]
    public GameObject tpanel;
    public GameObject bfPanel;
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        boatInteraction();
        if (l)
            StartGame();
    }
    void boatInteraction()
    {
        boatDistance = transform.position - boat.transform.position;
        if (boatDistance.sqrMagnitude < 1.7)
        {

            if (Input.GetKeyDown(KeyCode.F))
            {
                boat.GetComponent<Boat>().enabled = true;
                textBoat.SetActive(false);
            }
            if (boatDistance.sqrMagnitude != 0)
            {
                textBoat.SetActive(true);
            }
        }
        else
            textBoat.SetActive(false);
    }
    void StartGame()
    {
        s.SetActive(true);
        info = animators.GetCurrentAnimatorStateInfo(0);//持续获取动画进度
        
        if (info.normalizedTime >= 0.9)
        {
            man.SetActive(true);
            AudioCollider.SetActive(true);
            Destroy(s);
            Destroy(trigger);
            Destroy(trigger2);
            im.SetActive(true);
            transform.position = new Vector3(-3.369774f, -18.64286f, 0);
            Invoke("Close", time);
            floor.SetActive(true);
        }
        l = false;
    }
    void Close()
    {
        im.SetActive(false);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Trigger")
        {
            l = true;
            
        }
        if (collision.gameObject.tag == "bf")
        {
           // bfPanel.SetActive(true);
        }
       
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "tp"&&GameManager.instance.burn!=6)
            tpanel.SetActive(true);
        if (other.tag == "enter1")
            animator.enabled = true;
        if (other.tag == "ftrigger")
        {
            GameManager.instance.Begin();
        }
    }
}
