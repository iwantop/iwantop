using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagInduced : MonoBehaviour
{
    public GameObject Left;
    public MonoBehaviour Disappearing1;
    public MonoBehaviour Disappearing2;
    public MonoBehaviour move;
    public GameObject panel;

    public void Update()
    {
        if (Left == null)
            move.enabled = true;
    }
    public void CanNotMove()
    {
        move.enabled = false;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Door")
        {
            if(Disappearing1 != null)
                Disappearing1.enabled = true;
            if(Disappearing2 != null)
                Disappearing2.enabled = true;
            panel.SetActive(true);
            Invoke("CanNotMove", 1);   
        }
    }
}
