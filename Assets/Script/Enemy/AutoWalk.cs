using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoWalk : MonoBehaviour
{
    public  GameObject player;
    private Animator animator;
    public  float distancewithplayer;
    AnimatorStateInfo info;
    private enum EnemyState
    {
        IDLE,      //装死
        SCREAM,    //嚎叫
        STAND,     //爬起
        RUN,       //追逐
        WALK,      //闲逛
        BITE       //进食
    }
    private bool canchange = false; //控制播放次数
    private EnemyState enemyState = EnemyState.IDLE;    //默认状态
    public float safedistance = 10.0f;
    public float fspeed;   //追击速度
    public float sspeed;   //巡逻速度
    public float turnspeed = 0.1f;  //转身速度
    private Quaternion targetqua;
    public  float[] randomnum = { 2000, 3000, 4000 }; //巡逻状态配置（暂时禁用）

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {     
        info = animator.GetCurrentAnimatorStateInfo(0);
        distancewithplayer = Vector2.Distance(player.transform.position, transform.position);
        distancewithplayer = distancewithplayer < 0 ? -distancewithplayer : distancewithplayer;  //取绝对值
        Distance();
        switch (enemyState)
        {
            case EnemyState.IDLE:
                if (animator != null)
                    animator.Play("zombie-idle");
                break;

            case EnemyState.SCREAM:
                if (animator != null)
                    animator.Play("zombie-screaming");
                break ;

            case EnemyState.STAND:
                if (animator != null)
                    animator.Play("zombie-standing up");
                break;

            case EnemyState.BITE:
                if (animator != null)
                    animator.Play("zombie-biting");
                break;
            case EnemyState.WALK:
                if (animator != null)
                    animator.Play("zombie-walking");
                break;
            case EnemyState.RUN:
                if (animator != null && distancewithplayer >= 4)
                    animator.Play("zombie-running");
                if(distancewithplayer >= 3)
                {                  
                    targetqua = Quaternion.FromToRotation(-Vector3.up, player.transform.position - transform.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetqua, turnspeed);
                    transform.position = Vector2.MoveTowards(transform.position, player.transform.position, Time.deltaTime * fspeed);
                }
                if(distancewithplayer < 4)
                {
                    animator.Play("zombie-biting");
                }
                break;
        }
    }

    void Distance()
    {    
        if(distancewithplayer <= safedistance)
        {
            
            if (!canchange)
                enemyState = EnemyState.STAND;
            if((info.normalizedTime >= 1.0f) && info.IsName("zombie-standing up"))
            {
                enemyState = EnemyState.SCREAM;
                canchange = true;
            }
            if ((info.normalizedTime >= 0.9f) && info.IsName("zombie-screaming"))
            {
                animator.speed = 0.3f;
            }
            if ((info.normalizedTime >= 1.0f) && info.IsName("zombie-screaming"))
            {
                animator.speed = 1;
                enemyState = EnemyState.RUN;

            }
        }
        else
        {
            enemyState = EnemyState.IDLE;
            canchange=false;
        }
            
    }

    void RandomAct()
    {
        float rannum = Random.Range(0f, randomnum[0] + randomnum[1] + randomnum[2]);
        if(rannum <= randomnum[0])
        {

        }
        else if(rannum < randomnum[0] + randomnum[1])
        {

        }
        else if(rannum >= randomnum[0] + randomnum[1])
        {

        }
    }

    public void AttackAudio()
    {
        SoundManager.BlackMonsterAttackAudio();
    }
    public void ScreamAudio()
    {
        SoundManager.BlackMonsterScreamAudio();
    }
    public void WalkAudio()
    {
        SoundManager.BlackMonsterWalkAudio();
    }
}
