using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDanmu : MonoBehaviour
{
    public GameObject danmu;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(danmuAttack());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator danmuAttack()
    {
        yield return new WaitForSeconds(3.2f);
        for (int ii = 0; ii < 3; ii++)
        {
            for (int i = 0; i < 360; i += 40)
            {
                float x = 1 * Mathf.Cos(i * Mathf.Deg2Rad);
                float y = 1 * Mathf.Sin(i * Mathf.Deg2Rad);

                GameObject newdanmu = Instantiate(danmu);
                newdanmu.transform.position = new Vector3(x, y, 0) + transform.position;

                newdanmu.transform.eulerAngles = new Vector3(0, 0, i - 90);
            }
            SoundManager.NullMonsterDanmuAudio();
            yield return new WaitForSeconds(2);

        }
    }
}
