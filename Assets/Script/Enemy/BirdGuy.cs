using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdGuy : MonoBehaviour
{
    private PlayerData playerdata;
    private GameObject player;
    public  bool ifboss=false;
    public float distance;
    public float moveSpeed;
    public int dam;
    public int hp;
    float x;
    private Animator animator;
    Vector2 differ;
    [Header("Hurt")]
    private SpriteRenderer sp;
    public float hurtLength;
    private float hurtCount; 
    [System.Serializable]
    class SaveData2
    {
        public int shp;
        public Vector3 playertansform;
        
    }
    const string BIRDGUY_DATA_NAME = "bgData1.cunchu";
    
    void Awake()
    {
        animator = GetComponent<Animator>();
        sp = GetComponent<SpriteRenderer>();
        
        x = transform.position.x;
        
        if (hp <= 0)
        {
            SoundManager.BirdGuyDeathAudio();
            save();
            Destroy(gameObject);
            
        }
        
        player = GameObject.FindGameObjectWithTag("Player").gameObject;
        playerdata = player.GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {
        differ = new Vector2(player.transform.position.x - transform.position.x, player.transform.position.y - transform.position.y);
        if (!ifboss)
        {
            if (differ.sqrMagnitude < distance && differ.sqrMagnitude > 8)
                followPlayer();
            else if (differ.sqrMagnitude < 8)
            {
                animator.SetTrigger("at");
            }
            else
                animator.SetTrigger("walkb");
        }
        else if (ifboss)
        {
            if (differ.sqrMagnitude > 8)
                followPlayer();
        }
        if (hp <= 0)
        {
            SoundManager.BirdGuyDeathAudio();
            save();
            Destroy(gameObject);
        }
        if (hurtCount <= 0)
            sp.material.SetFloat("_FlashAmount", 0);
        else
            hurtCount -= Time.deltaTime;
    }
    void followPlayer()
    {

        if (differ.sqrMagnitude < 5)
            animator.SetTrigger("at");
        else
        {
            animator.SetTrigger("atb");
            animator.SetTrigger("walk");      
        }
        transform.position= Vector2.MoveTowards(transform.position, player.transform.position, moveSpeed*Time.deltaTime);
        float rotz = Mathf.Atan2(differ.y, differ.x) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0, 0, rotz-90);
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
            playerdata.playerDam(dam);
        if (other.tag == "bullet")
        { 
            hp -= 10;
            SoundManager.BulletAudio();
            HurtShader();
        }
        if (other.tag == "bigBullet")
        {
            hp -= 40;
            SoundManager.BigBulletAudio();
            HurtShader();
        }
        if (other.tag == "Slash")
        {
            hp -= 8;
            SoundManager.SlashAudio();
            HurtShader();
        }
        if (hp <= 0)
        {
            SoundManager.BirdGuyDeathAudio();
            save();
            Destroy(gameObject);

        }
        transform.position = transform.position - new Vector3(differ.x, differ.y, 0).normalized * 1.8f;
    }
    SaveData2 SavingData()
    {
        var saveData = new SaveData2();
        saveData.shp = hp;
        saveData.playertansform = transform.position;
        return saveData;
    }
    void GetData(SaveData2 saveData)
    {
        hp = saveData.shp;
        transform.position = saveData.playertansform;
    }
    public void save()
    {
        //SaveSystem.SaveByJson(BIRDGUY_DATA_NAME+x, SavingData());
        PlayerPrefs.SetInt("bg" + x, hp);
        PlayerPrefs.Save();
    }
    public void load()
    {
        //var saveData = SaveSystem.LoadFromJson<SaveData2>(BIRDGUY_DATA_NAME + x);
        //GetData(saveData);
        hp = PlayerPrefs.GetInt("bg" +x);
        if (hp <= 0)
        {
            save();
            Destroy(gameObject);

        }
    }
    private void HurtShader()
    {
        sp.material.SetFloat("_FlashAmount", 1);
        hurtCount = hurtLength;
    }

    public void AttackAudio()
    {
        SoundManager.BirdGuyAttackAudio();
    }
    public void WalkAudio()
    {
        SoundManager.BirdGuyWalkAudio();
    }
}
