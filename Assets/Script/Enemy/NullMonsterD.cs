using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NullMonsterD : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(7f * Time.deltaTime, 0, 0);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Untagged")
            Destroy(gameObject);
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerData>().playerDam(4);
            Destroy(this.gameObject);
        }
        
    }
}
