using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NullMonster : MonoBehaviour
{
    public GameObject player;
    public GameObject danmu;
    
    public GameObject tree;
    public GameObject FOG;
    public GameObject dia;
    public int hp;
    public float time;
    // Start is called before the first frame update
    void Start()
    {
        if (hp <= 0)
            Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if ((transform.position - player.transform.position).sqrMagnitude > 360)
            return;
        time++;
        if (time == 200)
            time = 0;
        if(time==10)
            StartCoroutine(danmuAttack());
        if (time == 80)
            StartCoroutine(treeAttack());
        if (hp <= 0)
        {
            dia.SetActive(true);
            SoundManager.NullMonsterDeathAudio();
            Destroy(FOG);
            Destroy(gameObject);
        }
    }
    float angleFix()
    {
        return Mathf.Atan2(player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * Mathf.Rad2Deg;  
    }
    IEnumerator danmuAttack()
    {
            for (int ii = 0; ii < 3; ii++)
            {
                for (int i = -60; i < 60; i += 15)
                {
                    float x = 1 * Mathf.Cos(i * Mathf.Deg2Rad);
                    float y = 1 * Mathf.Sin(i * Mathf.Deg2Rad);

                    GameObject newdanmu = Instantiate(danmu);
                    newdanmu.transform.position = new Vector3(x, y, 0) + transform.position;

                    newdanmu.transform.eulerAngles = new Vector3(0, 0, i - 90);
                }
            SoundManager.NullMonsterDanmuAudio();
                yield return new WaitForSeconds(2);

            }
    }
    IEnumerator treeAttack()
    {
        GameObject treep = Instantiate(tree, transform.position,Quaternion.Euler(0,0,angleFix()));
        SoundManager.NullMonsterTreeAudio();
        yield return new WaitForSeconds(1.5f);
        NULLMonstertr nULLMonstertr = treep.GetComponent<NULLMonstertr>();
        nULLMonstertr.v = -12f;
        yield return new WaitForSeconds(1.8f);
        Destroy(treep);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "bullet")
        {
            hp -= 10;
            SoundManager.BulletAudio();
        }
        if (other.tag == "bigBullet")
        {
            hp -= 40;
            SoundManager.BigBulletAudio();
        }
        if (other.tag == "Slash")
        {
            hp -= 6;
            SoundManager.SlashAudio();
        }
    }
    public void save()
    {
        PlayerPrefs.SetInt("H", hp);
        PlayerPrefs.Save();
    }
    public void load()
    {
        hp = PlayerPrefs.GetInt("H");
        if (hp <= 0)
            Destroy(gameObject);
    }
}
