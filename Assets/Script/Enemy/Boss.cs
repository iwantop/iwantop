using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public GameObject player;
    public GameObject[] fire;
    public GameObject end;
    public GameObject birdguy;
    public GameObject ss;
    public GameObject bexp;
    public GameObject danmu;
    public AudioSource audioSource;
    public GameObject sound;
    public int i = 0;
    public int hpb;
    bool isburn = true;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        BOSSHealth.hpCurrent = hpb;
        i++;
        if (i == 1800)
            i = 0;
        if (i == 40 || i == 300 && hpb > 0)
            bird();

        if (i == 600 || i == 1200 && hpb > 0)
            sss();
        if (i == 450 || i == 1500 && hpb > 0)
            StartCoroutine(exp());
        if (i == 1760)
            StartCoroutine(danmuAttack());
        if (hpb <= 0 && isburn)
        {
            audioSource.Play();
            StartCoroutine(fire1());
        }
    }
    IEnumerator fire1()
    {
        isburn = false;
        foreach (var fi in fire)
        {
            fi.SetActive(true);
            yield return new WaitForSeconds(0.5f);
        }
        end.SetActive(true);
        sound.SetActive(false);
    }
    IEnumerator danmuAttack()
    {
        for (int ii = 0; ii < 3; ii++)
        {
            for (int i =0; i < 360; i += 90)
            {
                float x = 1 * Mathf.Cos(i * Mathf.Deg2Rad);
                float y = 1 * Mathf.Sin(i * Mathf.Deg2Rad);

                GameObject newdanmu = Instantiate(danmu);
                newdanmu.transform.position = new Vector3(x, y, 0) + transform.position;

                newdanmu.transform.eulerAngles = new Vector3(0, 0, i - 90);
            }
            SoundManager.NullMonsterDanmuAudio();
            yield return new WaitForSeconds(2);

        }
    }
    void bird()
    {
        SoundManager.BossCreateAudio();
        for (int i = 0; i < 3; i++)
        {
            GameObject bi = Instantiate(birdguy, transform.position + new Vector3(i * 6f, 0, 0), Quaternion.identity);
            bi.GetComponent<BirdGuy>().ifboss = true;
        }        
    }
    void sss()
    {
        SoundManager.BossCreateAudio();
        for (int i = 0; i < 3; i++)
        {
            GameObject s = Instantiate(ss, transform.position + new Vector3(-6 + 6 * i, 0, 0), Quaternion.identity);
        }
    }
    IEnumerator exp()
    {
        for (int i = 0; i < 3; i++)
        {
            GameObject ex = Instantiate(bexp, player.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(1.8f);
        }

    }
    public void save()
    {
        PlayerPrefs.SetInt("Boss", hpb);
        PlayerPrefs.Save();
    }
    public void load()
    {
        hpb = PlayerPrefs.GetInt("Boss");
    }
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "bullet")
        {
            hpb -= 10;
            SoundManager.BulletAudio();
        }
        if (other.tag == "bigBullet")
        {
            hpb -= 40;
            SoundManager.BigBulletAudio();
        }
        if (other.tag == "Slash")
        {
            hpb -= 10;
            SoundManager.SlashAudio();
        }
    }
}