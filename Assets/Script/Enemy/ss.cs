using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ss : MonoBehaviour
{
    private PlayerData playerdata;
    private GameObject player;
    
    public float moveSpeed;
    public int dam;
    public int hp;
    private Animator animator;
    Vector2 differ;

    [Header("Hurt")]
    private SpriteRenderer sp;
    public float hurtLength;
    private float hurtCount;
    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").gameObject;
        playerdata = player.GetComponent<PlayerData>();
        sp = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        differ = new Vector2(player.transform.position.x - transform.position.x, player.transform.position.y - transform.position.y);
        if(differ.sqrMagnitude>6&&hp>0)
        followPlayer();
        
        if (hp <= 0)
        {
            SoundManager.SsDeathAudio();
            animator.SetTrigger("die");

        }
        if (hurtCount <= 0)
            sp.material.SetFloat("_FlashAmount", 0);
        else
            hurtCount -= Time.deltaTime;
    }
    void followPlayer()
    {
        
        if (differ.sqrMagnitude < 9)
            animator.SetTrigger("at");
        else
            animator.SetTrigger("atb");
        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, moveSpeed * Time.deltaTime);
        float rotz = Mathf.Atan2(differ.y, differ.x) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0, 0, rotz - 90);
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
            playerdata.playerDam(dam);
        if (other.tag == "bullet")
        {
            hp -= 10;
            SoundManager.BulletAudio();
            HurtShader();
        }
        if (other.tag == "bigBullet")
        {
            hp -= 40;
            SoundManager.BigBulletAudio();
            HurtShader();
        }
        if (other.tag == "Slash")
        {
            hp -= 10;
            SoundManager.SlashAudio();
            HurtShader();
        }
        if (hp <= 0)
        {

            animator.SetTrigger("die");
            Invoke("dd", 1.3f);

        }
        transform.position = transform.position - new Vector3(differ.x, differ.y, 0).normalized * 1.8f;
    }
    public void dd()
    {
        Destroy(gameObject);
    }
    private void HurtShader()
    {
        sp.material.SetFloat("_FlashAmount", 1);
        hurtCount = hurtLength;
    }

    public void AttackAudio()
    {
        SoundManager.SsAttackAudio();
    }
    public void WalkAudio()
    {
        SoundManager.SsWalkAudio();
    }
}
