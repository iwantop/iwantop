using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BOSSHealth : MonoBehaviour
{
    private Image image;
    public int hpMax;
    public static int hpCurrent;
    void Start()
    {
        hpCurrent = hpMax;
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        image.fillAmount = (float)hpCurrent / (float)hpMax;
    }
}
