using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;//序列化，转换成二进制数据

public class InventorySave : MonoBehaviour
{
    public Inventory mybag;

    public void Save()
    {
        Debug.Log(Application.persistentDataPath);

        if (!Directory.Exists(Application.persistentDataPath+"/Inventory_Data"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/Inventory_Data");
        }

        BinaryFormatter formatter = new BinaryFormatter();

        FileStream file = File.Create(Application.persistentDataPath + "/Inventory_Data/inventory.txt");

        var json = JsonUtility.ToJson(mybag);

        formatter.Serialize(file, json);

        file.Close();
    }

    public void Load()
    {
        BinaryFormatter bf = new BinaryFormatter();

        if (File.Exists(Application.persistentDataPath + "/Inventory_Data/inventory.txt"))
        {
            FileStream file = File.Open(Application.persistentDataPath + "/Inventory_Data/inventory.txt",FileMode.Open);

            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), mybag);

            file.Close();
        }


    }

}
