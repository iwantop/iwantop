using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    AudioSource playerSource;
    AudioSource objectSource;
    AudioSource ambientSource;
    AudioSource enemySource;
    AudioSource backSource;
    public Slider slider;
    [SerializeField]
    private AudioClip[] walkInFloorAudios;
    [SerializeField]
    private AudioClip[] walkInGrassAudios;
    [SerializeField]
    private AudioClip[] walkInInfectionAudios;
    [SerializeField]
    private AudioClip[] walkInMudAudios;
    [SerializeField]
    private AudioClip[] walkInConcreteAudios;

    [SerializeField]
    private AudioClip[] shotAudios;
    [SerializeField]
    private AudioClip fireGunAudio;
    [SerializeField]
    private AudioClip fuAudio;
    
    [SerializeField]
    private AudioClip[] doorAudio;

    [SerializeField]
    private AudioClip[] birdGuyAttackAudios;
    [SerializeField]
    private AudioClip[] birdGuyWalkAudios;
    [SerializeField]
    private AudioClip birdGuydeathAudio;

    [SerializeField]
    private AudioClip nullMonsterDanmuAudio;
    [SerializeField]
    private AudioClip nullMonsterTreeAudio;
    [SerializeField]
    private AudioClip nullMonsterDeathAudio;

    [SerializeField]
    private AudioClip[] ssAttackAudios;
    [SerializeField]
    private AudioClip ssDeathAudio;
    [SerializeField]
    private AudioClip ssWalkAudio;

    [SerializeField]
    private AudioClip[] bulletHurtAudio;
    [SerializeField]
    private AudioClip[] bigBulletHurtAudio;
    [SerializeField]
    private AudioClip slashHurtAudio;

    [SerializeField]
    private AudioClip[] blackMonsterAttackAudios;
    [SerializeField]
    private AudioClip[] blackMonsterWalkAudios;
    [SerializeField]
    private AudioClip[] blackMonsterScreamAudios;
    [SerializeField]
    private AudioClip blackMonsterDeathAudios;

    [SerializeField]
    private AudioClip bossCreateAudios;

    [SerializeField]
    private AudioClip treeAudio;

    [SerializeField]
    private AudioClip dreadfulAudio;

    [SerializeField]
    private AudioClip jiTanAudio;
    [SerializeField]
    private AudioClip tongDaoAudio;
    [SerializeField]
    private AudioClip bossSwitchAudio;
    [SerializeField]
    public AudioClip house1Audio;
    [SerializeField]
    public AudioClip house2Audio;

    public AudioClip startBGM;
    public AudioClip treeBGM;
    public AudioClip jiTanBGM;
    public AudioClip tongDaoBGM;
    public AudioClip bossBGM;
    public AudioClip HouseBGM1;
    public AudioClip HouseBGM2;
    public AudioClip[] WaterBGM;

    public AudioClip birdAudio;
    public AudioClip startTransAudio;
    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);

        playerSource = gameObject.AddComponent<AudioSource>();
        objectSource = gameObject.AddComponent<AudioSource>();
        ambientSource= gameObject.AddComponent<AudioSource>();
        enemySource= gameObject.AddComponent<AudioSource>();
        backSource= gameObject.AddComponent<AudioSource>();
    }

    private void Start()
    {
        DreadAudio();
    }
    private void Update()
    {
        instance.playerSource.volume = 0.7f*slider.value;
        instance.ambientSource.volume = 0.7f*slider.value;
        instance.objectSource.volume =slider.value;
        instance.enemySource.volume = 0.7f*slider.value;
        instance.backSource.volume =  instance.slider.value;
    }

    public static void AmbientPause()
    {
        instance.ambientSource.Pause();
    }
    public static void BGMPause()
    {
        instance.backSource.Pause();
    }

    public static void StartTransAudio()
    {
        instance.ambientSource.clip = instance.startTransAudio;
        instance.ambientSource.Play();
    }

    public static void StartBk()
    {
        instance.backSource.clip = instance.startBGM;
        instance.backSource.loop = true;
        instance.backSource.Play();
    }
    public static void TreeBk()
    {
        instance.backSource.clip = instance.treeBGM;
        instance.backSource.loop = true;
        instance.backSource.Play();
    }
    public static void JiTanBk()
    {
        instance.backSource.clip = instance.jiTanBGM;
        instance.backSource.loop = true;
        instance.backSource.Play();
    }
    public static void TongDaoBk()
    {
        instance.backSource.clip = instance.tongDaoBGM;
        instance.backSource.loop = true;
        instance.backSource.Play();
    }
    public static void BossBk()
    {
        instance.backSource.clip = instance.bossBGM;
        instance.backSource.loop = true;
        instance.backSource.Play();
    }
    public static void House1Bk()
    {
        instance.backSource.clip = instance.HouseBGM1;
        instance.backSource.loop = true;
        instance.backSource.Play();
    }
    public static void House2Bk()
    {
        instance.backSource.clip = instance.HouseBGM2;
        instance.backSource.loop = true;
        instance.backSource.Play();
    }
    public static void WaterBk()
    {
        int index = Random.Range(0, instance.WaterBGM.Length);
        instance.backSource.clip = instance.WaterBGM[index];
        instance.backSource.loop = true;
        instance.backSource.Play();
    }

    public static void BirdAudio()
    {
        instance.ambientSource.clip = instance.birdAudio;
        instance.ambientSource.loop = true;
        instance.ambientSource.Play();
    }
    public static void TreeAudio()
    {
        instance.ambientSource.clip = instance.treeAudio;
        instance.ambientSource.loop = true;
        instance.ambientSource.Play();
    }
    public static void JiTanAudio()
    {
        instance.ambientSource.clip = instance.jiTanAudio;
        instance.ambientSource.loop = true;
        instance.ambientSource.Play();
    }
    public static void TongDaoAudio()
    {
        instance.ambientSource.clip = instance.jiTanAudio;
        instance.ambientSource.loop = true;
        instance.ambientSource.Play();
    }
    public static void BossSwitchAudio()
    {
        instance.ambientSource.clip = instance.bossSwitchAudio;
        instance.ambientSource.loop = true;
        instance.ambientSource.Play();
    }
    public static void House1Audio()
    {
        instance.ambientSource.clip = instance.house1Audio;
        instance.ambientSource.loop = true;
        instance.ambientSource.Play();
    }
    public static void House2Audio()
    {
        instance.ambientSource.clip = instance.house2Audio;
        instance.ambientSource.loop = true;
        instance.ambientSource.Play();
    }

    public static void PlayerOnFloorAudio()
    {
        int index = Random.Range(0, instance.walkInFloorAudios.Length);
        
        instance.playerSource.clip = instance.walkInFloorAudios[index];
        instance.playerSource.Play();
    }
    public static void PlayerOnGrassAudio()
    {
        int index = Random.Range(0, instance.walkInGrassAudios.Length);
        instance.playerSource.clip = instance.walkInGrassAudios[index];
        instance.playerSource.Play();
    }
    public static void PlayerOnInfectionAudio()
    {
        int index = Random.Range(0, instance.walkInInfectionAudios.Length);
        instance.playerSource.clip = instance.walkInInfectionAudios[index];
        instance.playerSource.Play();
    }
    public static void PlayerOnMudAudio()
    {
        int index = Random.Range(0, instance.walkInMudAudios.Length);
        instance.playerSource.clip = instance.walkInMudAudios[index];
        instance.playerSource.Play();
    }
    public static void PlayerOnConcreteAudio()
    {
        int index = Random.Range(0, instance.walkInConcreteAudios.Length);
        instance.playerSource.clip = instance.walkInConcreteAudios[index];
        instance.playerSource.Play();
    }

    public static void ShotAudio()
    {
        int index = Random.Range(0, instance.shotAudios.Length);
        instance.objectSource.clip = instance.shotAudios[index];
        instance.objectSource.Play();
    }
    public static void FireGunAudio()
    {
        instance.objectSource.clip = instance.fireGunAudio;
        instance.objectSource.Play();
    }
    public static void FuAudio()
    {
        instance.objectSource.clip = instance.fuAudio;
        instance.objectSource.Play();
    }

    public static void DoorOpenAudio()
    {
        int index = Random.Range(0, instance.doorAudio.Length);
        instance.objectSource.clip = instance.doorAudio[index];
        instance.objectSource.Play();
    }

    public static void BirdGuyAttackAudio()
    {
        int index = Random.Range(0, instance.birdGuyAttackAudios.Length);
        instance.enemySource.clip = instance.birdGuyAttackAudios[index];
        instance.enemySource.Play();
    }
    public static void BirdGuyWalkAudio()
    {
        int index = Random.Range(0, instance.birdGuyWalkAudios.Length);
        instance.enemySource.clip = instance.birdGuyWalkAudios[index];
        instance.enemySource.Play();
    }
    public static void BirdGuyDeathAudio()
    {
        instance.enemySource.clip = instance.birdGuydeathAudio;
        instance.enemySource.Play();
    }

    public static void NullMonsterDanmuAudio()
    {
        instance.enemySource.clip = instance.nullMonsterDanmuAudio;
        instance.enemySource.Play();
    }
    public static void NullMonsterTreeAudio()
    {
        instance.enemySource.clip = instance.nullMonsterTreeAudio;
        instance.enemySource.Play();
    }
    public static void NullMonsterDeathAudio()
    {
        instance.enemySource.clip = instance.nullMonsterDeathAudio;
        instance.enemySource.Play();
    }

    public static void SsAttackAudio()
    {
        int index = Random.Range(0, instance.ssAttackAudios.Length);
        instance.enemySource.clip = instance.ssAttackAudios[index];
        instance.enemySource.Play();
    }
    public static void SsDeathAudio()
    {
        instance.enemySource.clip = instance.ssDeathAudio;
        instance.enemySource.Play();
    }
    public static void SsWalkAudio()
    {
        instance.enemySource.clip = instance.ssWalkAudio;
        instance.enemySource.Play();
    }

    public static void BossCreateAudio()
    {
        instance.enemySource.clip = instance.bossCreateAudios;
        instance.enemySource.Play();
    }

    public static void BulletAudio()
    {
        int index = Random.Range(0, instance.bulletHurtAudio.Length);
        instance.objectSource.clip = instance.bulletHurtAudio[index];
        instance.objectSource.Play();
    }
    public static void BigBulletAudio()
    {
        int index = Random.Range(0, instance.bigBulletHurtAudio.Length);
        instance.objectSource.clip = instance.bigBulletHurtAudio[index];
        instance.objectSource.Play();
    }
    public static void SlashAudio()
    {
        instance.objectSource.clip = instance.slashHurtAudio;
        instance.objectSource.Play();
    }


    public static void BlackMonsterAttackAudio()
    {
        int index = Random.Range(0, instance.blackMonsterAttackAudios.Length);
        instance.enemySource.clip = instance.blackMonsterAttackAudios[index];
        instance.enemySource.Play();
    }
    public static void BlackMonsterWalkAudio()
    {
        int index = Random.Range(0, instance.blackMonsterWalkAudios.Length);
        instance.enemySource.clip = instance.blackMonsterWalkAudios[index];
        instance.enemySource.Play();
    }
    public static void BlackMonsterScreamAudio()
    {
        int index = Random.Range(0, instance.blackMonsterScreamAudios.Length);
        instance.enemySource.clip = instance.blackMonsterScreamAudios[index];
        instance.enemySource.Play();
    }
    public static void BlackMonsterDeathAudio()
    {
        instance.enemySource.clip = instance.blackMonsterDeathAudios;
        instance.enemySource.Play();
    }

    public static void DreadAudio()
    {
        instance.backSource.clip = instance.dreadfulAudio;
        instance.backSource.Play();
        instance.backSource.loop = true;
    }
    
}
