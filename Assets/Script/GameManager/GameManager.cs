using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("���״̬")]
    public bool nullState = true;
    public bool gunState =false;
    public bool fireState = false;
    public bool fuState = false;

    public Inventory myinventory;
    [Header("��������")]
    public GameObject bp;
    public GameObject tp;
    public GameObject key;
    public int burn;
    public int ib;
    public int shouji;
    public bool isCreate;

    [Header("Final")]
    public Animator animator;
    public GameObject zudang;
    public GameObject Boss;
    public GameObject BossHealthb;
    public GameObject fpanel;
    public GameObject ffpanel;
    public GameObject FinalTrigger;

    [Header("UI")]
    public GameObject escp;
    public static GameManager instance = null;

    public GameObject endgame;

    [System.Serializable]
    class saveData
    {
        public bool n;
        public bool g;
        public bool fu;
        public bool fire;
        public int burnning;
        public int ibb;
        public int sshouji;
    }
    const string GAME_DATA_NAME = "gameData1.cunchu";
    void Start()
    {
        if (instance == null)
            instance = this;
        else if (instance != null)
            Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (isCreate)
            return;
        else
            burnc();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //Time.timeScale = 0;
            
            escp.SetActive(true);
            
        }
        
    }
    public void burnc()
    {
        if (ib == 3)
        {
            if (burn != 6)
            {
                ib = 0;
                bp.SetActive(true);
                Invoke("close", 1.5f);
            }
            else if (burn == 6)
            {
                key.SetActive(true);
                isCreate = true;
            }
        }
    }
    void close()
    {
        bp.SetActive(false);
    }
    void fclose()
    {
        fpanel.SetActive(false);
    }
    void ffclose()
    {
        ffpanel.SetActive(false);
    }
    public void end()
    {

        endgame.SetActive(true);
    }
    /*   
    public void Final()
    {
        if (shouji < 12)
        {
            fpanel.SetActive(true);
            Invoke("fclose", 3f);
        }
        else
        {
            ffpanel.SetActive(true);
            Invoke("ffclose", 3f);
            
        }
    }
    
    */

    public void Final()
    {
        FinalTrigger.SetActive(true);
    }
    public void Begin()
    {

        zudang.SetActive(true);
        animator.SetTrigger("Shake");
        Boss.SetActive(true);
        BossHealthb.SetActive(true);
    }
    saveData SavingData()
    {
        var saveData1 = new saveData();
        saveData1.burnning = burn;
        saveData1.ibb = ib;
        saveData1.fu = fuState;
        saveData1.g = gunState;
        saveData1.fire = fireState;
        saveData1.n = nullState;
        saveData1.sshouji = shouji;
        return saveData1;
    }
    void GetData(saveData saveData1)
    {
        burn = saveData1.burnning;
        ib = saveData1.ibb;
        gunState = saveData1.g;
        fuState = saveData1.fu;
        fireState = saveData1.fire;
        nullState = saveData1.n;
        shouji = saveData1.sshouji;
    }
    public void save()
    {
        SaveSystem.SaveByJson(GAME_DATA_NAME, SavingData());
        
    }
    public void load()
    {
        var saveData1 = SaveSystem.LoadFromJson<saveData>(GAME_DATA_NAME);
        GetData(saveData1);
    }
}
