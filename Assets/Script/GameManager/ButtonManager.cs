using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    [Header("�洢")]
    public Shot shot;
    public BirdGuy birdGuy;
    public InventorySave inventorySave;
    public GameManager gameManager;
    public NullMonster nullMonster;
    public PlayerData playerData;
    public Gun2 gun2;
    public Inventory bag;
    public List<Item> items;
    public Boss boss;

    [Header("����")]
    public static bool ifRestart=false;
    public GameObject startPanel;
    public GameObject im;

    [Header("��ͣ")]
    public GameObject escp;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartNewGame()
    {
        Time.timeScale = 1;
        ifRestart = true;
        bag.itemList = items;
        InventoryManager.RefreshItem();
        im.SetActive(true);
        startPanel.SetActive(false);
        Invoke("dd", 3f);
    }
    public void StartLoadGame()
    {
        Time.timeScale = 1;
        ifRestart = true;
        InventoryManager.RefreshItem();
        im.SetActive(true);
        Load();
        startPanel.SetActive(false);
        Invoke("dd", 3f);
        escp.SetActive(false);
    }
    public void Continue()
    {
        Time.timeScale = 1;
        escp.SetActive(false);
        
    }
    public void Back()
    {
        ifRestart = true;
        InventoryManager.RefreshItem();
        SceneManager.LoadScene("Start");
    }
    public void exit()
    {
        ifRestart = true;
        Application.Quit();
    }
    void dd()
    {
        im.SetActive(false);
    }
    public void Save()
    {
        inventorySave.Save();
        shot.Save();
        birdGuy.save();
        gun2.save();
        nullMonster.save();
        playerData.save();
        gameManager.save();
        boss.save();
    }
    public void Load()
    {
        im.SetActive(true);
        ifRestart = true;
        inventorySave.Load();
        Invoke("dd", 3f);
        shot.Load();
        birdGuy.load();
        gun2.load();
        nullMonster.load();
        playerData.load();
        gameManager.load();
        boss.load();
    }
    public void RightChoice()
    {
        GameManager.instance.ib++;
        GameManager.instance.burn+=2;
    }
    public void WrongChoice()
    {
        GameManager.instance.ib++;
    }
}
