using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    
    #region JSON
    public static void SaveByJson(string saveFileName,object data)
    {
        var json = JsonUtility.ToJson(data);//将数据转换成json格式
        var path = Path.Combine(Application.persistentDataPath, saveFileName);//调用Application.persistentDataPath，自动创建存储区域

       
        try
        {
            File.WriteAllText(path, json);//创建新文件，并将json文本写入 接受路径参数，与写入文本内容参数
            #if UNITY_EDITOR
            Debug.Log(path+"ss");
            #endif  
        }
        catch(System.Exception exception) 
        {
            Debug.Log(path + "\n" + exception);
        }
    }

    public static T LoadFromJson<T>(string saveFileName)//接受泛型参数T，并返回相应泛型值，同时接受字符串参数，传递存档文件名称
    {
        var path = Path.Combine(Application.persistentDataPath, saveFileName);
        var json = File.ReadAllText(path);//读取到json格式的字符串
        var data = JsonUtility.FromJson<T>(json);//将json字符串转换成泛型数据

        return data;
    }

    #endregion
    #region Deleting
    public static void DeleteSaveFile(string saveFileName)
    {
        var path = Path.Combine(Application.persistentDataPath, saveFileName);
        File.Delete(path);
    }
    
    #endregion
}
