using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class text : MonoBehaviour
{
    private Text text1;
    [SerializeField] private float alpha;
    void Start()
    {
        text1 = GetComponent<Text>();
        StartCoroutine(FadeIn());
    }

    IEnumerator FadeIn()
    {
        text1.color = new Color(221, 101, 101, alpha);
        alpha = 0;
        while (text1.color.a<=1)
        {
            alpha += Time.deltaTime;
            text1.color = new Color(221, 101, 101, alpha);
            yield return new WaitForSeconds(0.12f);
        }
        while (text1.color.a >= 0)
        {
            alpha -= Time.deltaTime;
            text1.color = new Color(221, 101, 101, alpha);
            yield return new WaitForSeconds(0.12f);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
